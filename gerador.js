const dados = require('./dados.json')

const geraJogador = function(){
    const jogador = {
        "nome": dados.nome[parseInt(Math.random() * dados.nome.length)],
        "sobrenome": dados.sobrenome[parseInt(Math.random() * dados.sobrenome.length)],
        "idade": parseInt(Math.random() * 23) + 17,
        "posicao": dados.posicao[parseInt(Math.random() * dados.posicao.length)],
        "clube": dados.clube[parseInt(Math.random() * dados.clube.length)],
    }

    jogador.mensagem = `${jogador.nome} ${jogador.sobrenome} é um futebolista brasileiro de ${jogador.idade} anos que atua como ${jogador.posicao}. Atualmente defende o ${jogador.clube}.`

    return jogador
}

module.exports = geraJogador