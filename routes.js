const express = require('express')
const router = express.Router()
const geraJogador = require('./gerador')

router.get("/", function(req, res){
    res.send("Use a rota /api/gerador para gerar um novo jogador")
})

router.get('/gerador', function(req, res){
    const jogador = geraJogador()
    res.json(jogador)
})

module.exports = router