const express = require('express')
const app = express()
const routes = require('./routes.js')
const cors = require('cors')
const porta = process.env.PORT || 3000

app.use(cors()) // adicionar cabeçalho CORS nas respostas
app.get("/", function(req, res){
    res.send("Seja bem vindo ao gerador de jogadores. Use a rota /api/gerador para gerar um novo jogador")
})
app.use('/api', routes)

app.listen(porta, function(){
    console.log(`Servidor rodando na porta ${porta}`)
})